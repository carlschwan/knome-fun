const button = document.getElementById('button');
const button2 = document.getElementById('button2');

function foo()
{
    var randX = Math.floor(Math.random() * (window.innerWidth - 150));
    var randY = Math.floor(Math.random() * (window.innerHeight - 100));
    console.log(randX, randY);
    button.style.position = "absolute";
    button.style.left = randX + "px";
    button.style.top = randY + "px";
}

function foo2(event)
{
    event.preventDefault();
    button2.classList.remove('learn-more');
    var i = 10;
    var x = setInterval(function() {
        if (i === 0) {
              window.location = button.href;
              clearInterval(x);
        }
        button2.innerText = i;
        i -= 1;
    }, 1000);
}

button.addEventListener('mouseenter', foo);
button2.addEventListener('click', foo2);
